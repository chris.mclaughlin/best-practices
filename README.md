# Debugging PHP code within PHPStorm using XDebug 3 and Docker

### 1. Why?

![Breakpoints](/docs/breakpoints.png)

- Stop using `var_dum()` and `die()` all the time. 
- Able to set breakpoints for your code to exit via a click of the mouse.
- Easily view the source files and path your application takes by stepping through the debug window.

## 2. Setup

1. `git clone [https://gitlab.com/chris.mclaughlin/best-practices](https://gitlab.com/chris.mclaughlin/best-practices)` and `cd debugging` 
2. `docker-compose build`
3. `docker-compose up -d`
4. Confirm that PHPStorm’s Debug settings are correct under Settings → PHP → Debug (You’re checking that the Debug port is set to 9000,9003 - This is normally set by default)
5. Run → and Start Listening for PHP Debug Connections
6. Set A break point and Run your code!

7. ![Enable Server](/docs/connecting_to_phpstorm.png)